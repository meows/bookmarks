const FoldersNavigation = function({ folders }) {
   // takes folders:string[] and returns a breadcrumb navigation
   const style = {
      icon: {
         color: "lightgray",
         marginLeft: "5px",
      },
   };

   return (
      <nav className="ui large breadcrumb">
         {
            folders.map((item, index) => [
               <a className="section underlined">{item}</a>,
               index < (folders.length - 1) ? <i className="right chevron icon divider"></i> : null,
            ])
         }
      </nav>
   )
}
