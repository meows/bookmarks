import ReactDOM              from "react-dom"
import React                 from "react"
import { createStore }       from "redux"
import { createHashHistory } from "history"
import { Router, Route, Link, hashHistory, useRouterHistory } from "react-router"
// import * as data             from "./data.js";
// import * as Rx               from "@reactivex/rxjs";

// -----------------------------------------------------------------------------
// Render

const render = function(state) {
   const AppWrapper = function() { return AppView(state) }
   const appHistory = useRouterHistory(createHashHistory)({ queryKey: false })
   ReactDOM.render(
      (
         <Router history={appHistory}>
            <Route path="/"            component={AppWrapper} />
            <Route path="/settings"    component={SettingsView} />
            <Route path="/addBookmark" component={InputView} />
         </Router>
      ),
      document.getElementById('app')
   )
}

// -----------------------------------------------------------------------------
// Components

const AppView = function({ bookmarksData, interfaceData }) {
   return (
      <main id="container">
         <TopNavigation active="home" />
         <section id="content-container">
            <section id="left-container">
               <TopTagsNavigation folders={interfaceData.folders} />
               <BookmarkContainer bookmarks={bookmarksData} />
            </section>
            <SideNavigation tags={interfaceData.tags} />
         </section>
         <Footer />
      </main>
   )
}

const TopNavigation = function({ active }) {
   // takes active:string and returns the top navigation menu
   let a, b, c, d;
   a = b = c = d = "item"

   switch (active) {
      case "home"     : a = "item active"; break
      case "settings" : b = "item active"; break
      case "manage"   : c = "item active"; break
      case "add"      : d = "item active"; break
      default         : a = "item active"; break
   }

   return (
      <nav id="top" className="ui secondary pointing menu">
         <Link to="/"            className={ a }>home</Link>
         <Link to="/settings"    className={ b }>settings</Link>
         <Link to="#"            className={ c }>manage</Link>
         <Link to="/addBookmark" className={ d }>add bookmark</Link>
         <div className="right menu">
            <a className="item">logout</a>
         </div>
      </nav>
   )
}

// TODO: get state from outside
const TopTagsNavigation = function({ folders }) {
   const style = {
      ul: {
         display     : "flex",
         listStyle   : "none",
         paddingleft : 0,
      },
      li: {
         marginRight: "1ex",
      },
   };

   return (
      <nav id="TopTagsNavigation">
         <ul>
            {
               folders.map((item, index) => [
                  <li><a className="ui label" href="#">{item}<i className="close icon"></i></a></li>,
                  index < (folders.length - 1) ? <li className="divider">+</li> : null,
               ])
            }
         </ul>
      </nav>
   )
}

const SideNavigation = function({ tags }) {
   return (
      <nav id="right-container">

         <div className="ui fluid category search">
            <div className="ui icon input" id="search">
               <input type="text" placeholder="\&#32;&#32; to search" />
               <i className="search link icon"></i>
            </div>
         </div>

         <nav id="tags-container">
            <div id="tags-editor">
               <ul>
                  <li id="selected">top tags</li>
                  <li id="divider">•</li>
                  <li id="unselected">all tags</li>
               </ul>
               <hr />
            </div>
            <ul id="tags-list">
               { tags.map((item, index) => <li key={index}><a href="#">{item}</a></li>) }
            </ul>
         </nav>
      </nav>
   )
}

const BookmarkContainer = function({ bookmarks }) {
   return (
      <section id="bookmarks-container">
         { bookmarks.map((item, index) => <Bookmark key={index} bookmark={item} />) }
      </section>
   )
}

const Bookmark = function({ bookmark }) {
   const time  = bookmark.time.created
   const title = bookmark.title ? bookmark.title : "Untitled"
   const truncatedBody = function(bodyText = bookmark.about) {
      if (!Boolean(bodyText)) {
         return null
      }
      else if (bodyText.length <= 350) {
         return bodyText
      }
      else if (bodyText.length > 350) {
         return `${bodyText.substring(0, 351)}...`
      }
      else {
         return "Error in truncating text."
      }
   }();
   const months = {
      1  : 'January',
      2  : 'February',
      3  : 'March',
      4  : 'April',
      5  : 'May',
      6  : 'June',
      7  : 'July',
      8  : 'August',
      9  : 'September',
      10 : 'October',
      11 : 'November',
      12 : 'December',
   }

   return (
      <article className="bookmark">
         {/* ---- BOOKMARK ASIDE ---- */}
         <aside>
            <time className="time">{`${time.day}.${time.month}.${time.year}`}</time>
            <nav className="bookmark-editor">
               <ul className="underlined">
                  <li><a href="#">edit</a></li>
                  <li><a href="#">delete</a></li>
               </ul>
            </nav>
         </aside>
         {/* ---- BOOKMARK BODY ---- */}
         <section className="bookmark-body">
            <h4 className="title underlined"><a href={bookmark.url} className="bookmark-title">{title}</a></h4>
            { Boolean(truncatedBody) ? <p>{truncatedBody}</p> : null }
            <nav>
               <ul className="bookmark-tags underlined">
                  { Boolean(bookmark.tags) ? bookmark.tags.map((item, index) => <li key={index}><a href="#">{item}</a></li>) : null }
               </ul>
            </nav>
         </section>
      </article>
   )
}

const Pagination = function() {
   return (
      <nav className="ui pagination menu">
         <a className="active item">1</a>
         <a className="item">2</a>
         <a className="item">3</a>
         <a className="item">4</a>
         <div className="disabled item">...</div>
      </nav>
   )
}

const Footer = function() {
   return (
      <footer id="footer">
         <Pagination />
         <ul className="underlined">
            <li><a href="#">help</a></li>
            <li>•</li>
            <li><a href="#">about</a></li>
            <li>•</li>
            <li><a href="#">terms of service</a></li>
         </ul>
      </footer>
   )
}

const SlimFooter = function() {
   // footer without pagination
   const style = {
      footer: {
         hr: {
            marginTop    : "50px",
            marginBottom : "20px",
            border       : 0,
            height       : 0,
            borderTop    : "1px solid rgba(0, 0, 0, 0.1)",
            borderBottom : "1px solid rgba(255, 255, 255, 0.3)",
         },
      },
   };

   return (
      <footer id="footer">
         <ul className="underlined">
            <li><a href="#">help</a></li>
            <li>•</li>
            <li><a href="#">about</a></li>
            <li>•</li>
            <li><a href="#">terms of service</a></li>
         </ul>
      </footer>
   )
}

// ---- Other Pages ------------------------------ //

const InputView = function() {
   const style = {
      main: {
         display       : "flex",
         flexDirection : "column",
         minHeight     : "98vh",
      },
      content: {
         flex: 1,
      },
   };

   return (
      <main id="container" style={style.main}>
         <section style={style.content}>
            <TopNavigation active="add" />
            <InputForm />
         </section>
         <SlimFooter />
      </main>
   )
}

const InputForm = function() {
   const saveForm = function() {
      store.dispatch({
         type: "NEW_BOOKMARK",
         data: addBookmark(jsonConverter()),
      })
   }
   const cancelForm = function() {
      // TODO: update to use React Router history
      window.history.back()
   }
   const style = {
      form: {
         display      : "table",
      },
      row: {
         display: "table-row",
      },
      cell: {
         display       : "table-cell",
         paddingRight  : "20px",
         textAlign     : "right",
         verticalAlign : "top",
         fontWeight    : "bold"
      },
      input: {
         width        : "400px",
         marginBottom : "20px",
      },
      button: {
         marginRight: "1.5ex",
      },
      section: {
         marginTop: "3ex",
      },
   };

   return (
      <section id="inputForm" style={style.section}>
         <form name="bookmarkForm" style={style.form}>
            <div style={style.row}>
               <label style={style.cell}>URL</label>
               <input name="url" type="text" style={style.input} />
            </div>

            <div style={style.row}>
               <label style={style.cell}>Title</label>
               <input name="title" type="text" style={style.input} />
            </div>

            <div style={style.row}>
               <label style={style.cell}>About</label>
               <textarea rows="4" name="about" style={style.input}></textarea>
            </div>

            <div style={style.row}>
               <label style={style.cell}>Tags</label>
               <input name="tags" type="text" style={style.input} />
            </div>

            <div style={style.row}>
               <div style={style.cell}></div>
               <button onClick={saveForm}  type="button" style={style.button}>Submit</button>
               <button onClick={cancelForm} type="button" style={style.button}>Cancel</button>
            </div>
         </form>
      </section>
   )
}

const SettingsView = function() {
   return (
      <main id="container">
         <TopNavigation active="settings" />
         <SettingsForm />
      </main>
   )
}

const SettingsForm = function() {
   const style = {
      section: {
         display       : "flex",
         flexDirection : "column",
      },
      div: {
         marginBottom: "2ex",
      },
   };

   return (
      <section style={style.section}>
         <div className="ui toggle checkbox" style={style.div}>
            <input type="checkbox" name="one" />
            <label>Example feature</label>
         </div>
         <div className="ui toggle checkbox" style={style.div}>
            <input type="checkbox" name="two" />
            <label>Example feature</label>
         </div>
      </section>
   )
}

// -----------------------------------------------------------------------------
// State

const initialState = {
   status  : "IDLE",
   payload : null,
}

const networkReducer = function(state = initialState, action) {
   switch (action.type) {
      case "REQUESTING":
         return {
            status : action.type,
            data   : null,
         }
      case "SUCCESS":
         return {
            status : action.type,
            data   : action.data,
         }
      case "TIMEOUT":
         return {
            status : action.type,
            data   : action.data,
         }
      case "ERROR":
         return {
            status : action.type,
            data   : action.data,
         }
      default:
         return state
   }
}

const networkStore = createStore(networkReducer);

const observer = function(state = networkStore.getState()) {
   switch (state.status) {
      case "SUCCESS":
         console.log("State Change: ", state)
         render(state.data)
         break;
      default:
         console.log("State Change: ", state)
         break;
   }
}

networkStore.subscribe(observer)

// initial state report
console.log("State Change: ", networkStore.getState())

// -----------------------------------------------------------------------------
// Network Functions

const dispatcher = function(type, data) { networkStore.dispatch({ type, data }) }

const timer = new Promise(
   function(resolve, reject) {
      setTimeout(reject, 6000, "timeout during fetch() promise race")
   }
)

// getter :: () -> IO -> dispatcher()
const getter = function() {
   const url = "https://gist.githubusercontent.com/meows/1ea8ee8cc070bf5fa021fb5ba5039a45/raw/6e9bb0125c045fa860d92156744ee1c756a83fad/dataTest.json";
   dispatcher("REQUESTING", null)

   return fetch(url)
      .then(x => x.json())
      .catch(err => { dispatcher("ERROR", err) })
   ;
}

// faceFetch :: () -> IO -> dispatcher()
const raceFetch = function() {
   Promise.race([
      getter(),
      timer,
   ])
   .then(value => { dispatcher("SUCCESS", value) })
   .catch(err => { dispatcher("TIMEOUT", err) });
}

raceFetch()

