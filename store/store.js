import { createStore } from 'redux'

//------------------------------------------------------------------------------
// Model Data

let modelAction = {
  type    : "",
  payload : {},
}

// -----------------------------------------------------------------------------
// Network Functions

function getter() {
  const url = "https://gist.githubusercontent.com/meows/1ea8ee8cc070bf5fa021fb5ba5039a45/raw/e2ed66ac9af655e634e5cd6b45721d668d7523d7/dataTest.json"
  networkStore.dispatch({
    type    : "REQUESTING",
    payload : null,
  })

  return fetch(url)
    .then(x => x.json())
    .then(x => console.log(x))
    .then(x => networkStore.dispatch({
      type: "SUCCESS",
      data: x,
    }))
    .catch(err => console.log(err))
  ;
}

// -----------------------------------------------------------------------------
// Reducer

const initialState = {
  status: "IDLE",
  payload: null,
}

function networkReducer(state = initialState, action) {
  switch (action.type) {
    case "REQUESTING":
      return {
        status : action.type,
        data   : null,
      }
    case "SUCCESS":
      return {
        status : action.type,
        data   : action.data,
      }
    case "ERROR":
      return {
        status : action.type,
        data   : action.error,
      }
  }
}

let networkStore = createStore(networkReducer);
console.log(networkStore.getState())
