const timer = new Promise(
   function(resolve, reject) {
      setTimeout(reject, 6000, "timeout during fetch() promise race")
   }
)

const getter = function() {
   const url = "http://httpbin.org/get"
   dispatch("REQUESTING", null);

   return fetch(url)
      .then(x => x.json())
      .catch(err => { dispatch("ERROR", err) })
   ;
}

const raceFetch = function() {
   return Promise.race([
      getter(),
      timer,
   ])
   .then(value => { dispatch("SUCCESS", value) })
   .catch(err => { dispatch("TIMEOUT", err) });
}

const dispatch = function(type, data) {
   postMessage({ type, data });
}

raceFetch();